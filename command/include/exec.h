#ifndef __EXEC_H__
#define __EXEC_H__

#ifdef __cplusplus
extern "C"
{
#endif
    void outprint(char const *argv);
#ifdef __cplusplus
}
#endif

#endif
